--- Tuesday March 29, 2016 ---

I'm learning how to commit to a repository with git, and also trying out Foundation 6 templating.

Each HTML file in here is intended to be a separate template. The default index.html from the standard pre-compiled Foundation 6 distribution has been omitted by the .gitignore file. The other files come from a likely outdated Foundation distribution included only to keep the repository self-contained with all dependencies.

Very little of the HTML in these templates will be original to me, most of it being copied from the Zurb Code Library:
	http://zurb.com/building-blocks

Naturally, anyone who wants to clone this repo is free to use any templates in it for any purpose.